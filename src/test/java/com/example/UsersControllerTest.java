package com.example;

import com.example.Repository.UsersRepository;
import com.example.service.UsersService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
//@AutoConfigureMockMvc
public class UsersControllerTest {

    private MockMvc mvc;

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private UsersController usersController;

//    @MockBean
//    private UsersRepository usersRepository;

    @MockBean
    private UsersService usersService;

    @Before
    public void before() throws Exception {
        this.mvc = MockMvcBuilders.standaloneSetup(usersController).build();
    }

    @Test
    public void testShow() throws Exception {
        List<User> list = new ArrayList<>();
        User user1 = new User();
        user1.setId(1);
        user1.setName("hoge");
        list.add(user1);
        when(usersService.getById(12)).thenReturn(list);

        mvc.perform(get("/users/12"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                ;
    }
}